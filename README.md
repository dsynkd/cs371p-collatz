# CS371p: Object-Oriented Programming Collatz Repo

* Name: Darren Sadr

* EID: ss65728

* GitLab ID: dsynkd

* HackerRank ID: dsynkd

* Git SHA: 1e5045cf723ff8c91d1b8c8aa52449d9033331be

* GitLab Pipelines: https://gitlab.com/dsynkd/cs371p-collatz/pipelines

* Estimated completion time: 5-10 hours

* Actual completion time: 12 hours

* Comments: interesting project
